#pragma once

#include <stdlib.h>
#include <iostream>
#include <string>

#include "player.hpp"

// Class used for a human player, for testing purposes
class Human : public Player {
    private:

    // Get the type of the piece from a char
    ElementType type_from_string(std::string c);

    // Prints piece placement instructions
    void print_placement_instructions();

    public:

    // Constructor
    Human(TeamBoard *board): Player(board) {};

    // Function used to set up the pieces
    void setup_pieces();

    // Function used to quickly set up pieces
    void setup_pieces(bool quick);

    // Function used to start a turn.
    // The turn ends when the function resolves.
    void start_turn();
};