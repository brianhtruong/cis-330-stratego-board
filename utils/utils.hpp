#pragma once

#include <stdlib.h>
#include <iostream>
#include <string>

// The result of an encounter on the board
enum class Result {
    died,
    tied,
    failed,
    success
};

// Directions pieces can move
enum class Direction {
    up,
    down,
    left,
    right,
    unknown
};

// Types of teams, used for assignment and win checks
enum class Team {
    red,
    blue,
    none,
    both
};

// Types of board elements
enum class ElementType {
    spy = 1,
    scout,
    miner,
    nine,
    ten,
    bomb,
    flag,
    deadzone,
    unknown,
    empty
};

// Actual element representation
struct Element {
    ElementType type = ElementType::unknown;
    Team team = Team::none;
    bool alive = false;
    ElementType killer = ElementType::unknown;
    int x = 0;
    int y = 0;
};

// Report of a piece interaction
struct Report {
    Result result = Result::failed;
    ElementType encountered = ElementType::unknown;
};

// Overloaded printing

// Print element type as char
std::ostream & operator<<(std::ostream &s, ElementType piece);

// Matrix operations
namespace matrix_utils {
    // Matrix allocation function
    template <typename T>
    T ** allocate_matrix(int rows, int cols, T init);

    // Matrix free function
    template <typename T>
    void free_matrix(int rows, T ** matrix);
}


#include "utils.ipp"