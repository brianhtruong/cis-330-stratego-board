#include <stdlib.h>
#include <iostream>
#include <string>

#include <random>
#include <cstdlib>
#include <time.h>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "board/board.hpp"
#include "utils/human.hpp"
#include "utils/player.hpp"

using namespace std;

int main() {
    // Instantiate the boards
    Board *game_board = new Board();
    TeamBoard *r_board = new TeamBoard(game_board, Team::red);
    TeamBoard *b_board = new TeamBoard(game_board, Team::blue);

    // Instantiate players
    // When you have an AI, swap out the Human class for your AI class
    Human *red = new Human(r_board);
    Human *blue = new Human(b_board);

    // Randomly choose who to start
    default_random_engine generator; 
    bernoulli_distribution d(0.50);
    bool red_turn = d(generator);

    // Set up the game pieces
    red->setup_pieces(true);
    blue->setup_pieces(true);

    // Game loop
    int turn;
    int max_turns = 500; // Set a maximimum amount of turns to play
    for (turn = 0; turn < max_turns; ++turn) {
        // Clear the screen
        cout << string(100, '\n');

        // Check for a winner
        if (game_board->check_winner()) {
            game_board->print_state();
            break;
        }

        // Do the turn
        game_board->print_dead();
        std::cout << std::endl;
        try {
            if (red_turn) {
                std::cout << "RED PLAYER TURN" << std::endl << std::endl;
                game_board->print_state(Team::red);
                red->start_turn();
            } else {
                std::cout << "BLUE PLAYER TURN" << std::endl << std::endl;
                game_board->print_state(Team::blue);
                blue->start_turn();
            }
        } catch (const exception& err) {
            cerr << (red_turn ? "Red" : "Blue") << " board error: " << err.what() << endl;
            break;
        } catch (...) {
            cerr << (red_turn ? "Red" : "Blue") << " board unknown error" << endl;
            break;
        }

        // Swap players for the next turn
        red_turn = !red_turn;

        // Temporary delay due to speed
        this_thread::sleep_for(1s);
    }

    // Stalemate notification
    if (turn == 500) std::cerr << "Players took too long. Stalemate!" << std::endl;

    // Delete everything
    delete red;
    delete blue;
    delete b_board;
    delete r_board;
    delete game_board;

    return 0;
}