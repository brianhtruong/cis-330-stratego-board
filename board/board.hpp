#pragma once

#include "../utils/utils.hpp"

// Board class definition
// This is the global board, not to be used by a specific team
class Board {
private:

    // Static board elements
    Element * empty; // Empty space element
    Element * deadzone; // Deadzone element

    // Board
    Element * ** board;

    // Team the previously moved
    Team previous = Team::none;
    
    // Win condition trackers
    Team flag_captured = Team::none;
    int red_mobile = 7;
    int blue_mobile = 7;

    // Check if a path is clear
    bool check_path_clear(int x1, int y1, int x2, int y2);

    // Base check function
    bool check_boundary_deadzone(int x, int y);
    Result check_rank(Element *myPiece, Element *theirPiece);

    // Track number of pieces per team
    // The index zero is used to store the total amount of pieces
    int * red_piece_count;
    int * blue_piece_count;

    // Team pieces
    Element * red_pieces;
    Element * blue_pieces;

    // Checks to see if a piece can be placed
    bool check_if_placeable(Team team, ElementType piece);

    // Helper function: add deadzones to 10x10 board
    template <typename T>
    void add_deadzones(T ** board, T deadzone_representation);

    // sparse matrix representation of pieces
    int * **spm;

    // Returns a SPM 
    int * **spmr(Team team);

public:

    // Board constructor
    Board();

    // Base move function
    Report move(Team team, int pieceX, int pieceY, Direction dir, int spaces);

    // Base board state retrieval function
    // Returns a size 20 array with pieces from both teams
    // Remember to free when done reading state
    Element * state(Team team);

    // Base setup placement function
    bool place(Team team, ElementType piece, int x, int y);

    // Checks for a winner
    bool check_winner();

    // Prints the board's full state to console for human viewing
    void print_state();

    // Prints a size 20 state array
    void print_state(Element *state);

    // Prints a team specific matrix view
    void print_state(Team team);

    // Prints athe dead pieces
    void print_dead();

    // Returns a SPM 
    int * ** return_spmr(Team team){
        spmr(team);
        return spm;
    };

    // Board deconstructor
    ~Board();
};

// Team Board class definition
// This is the board use by a specific team,
// hiding the info/functions of the other team
class TeamBoard {
    private:

    // Reference to the true board
    Board * board;

    // Team assigned to this board
    Team team; 

public:
    
    // Team Board Constructor
    TeamBoard(Board *board, Team team): board(board), team(team) {};

    // Team specific place
    bool place(ElementType piece, int x, int y) {
        return board->place(team, piece, x, y);
    }

    // Team specific move
    Report move(int pieceX, int pieceY, Direction dir, int spaces) {
        return board->move(team, pieceX, pieceY, dir, spaces);
    }

    // Team specific state; remember to free when done reading state
    Element * state() {
        // It might be a good idea to maintain a 2D matrix
        // representing what is seen by the team.
        return board->state(team);
    }

    // Prints the current state of the board (from Team perspective)
    void print_state() {
        board->print_state(team);
    }

    // Getter for this board's assigned team
    Team get_team() {
        return team;
    }
};