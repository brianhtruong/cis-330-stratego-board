# Make file for the compiling the project

CXX = g++
CXXFLAGS = -g -Wall -Wextra -pedantic -std=c++14

BOARD_OBJ = obj/board.o obj/board_place.o obj/board_move.o obj/board_state.o
BOARD_DEP = board/board.hpp utils/utils.hpp obj

# The main executable
main: obj/main.o obj/utils.o obj/human.o $(BOARD_OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^

# The main object file
obj/main.o: main.cpp board/board.hpp utils/human.hpp obj
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Utils function implementation file
obj/utils.o: utils/utils.cpp utils/utils.hpp obj
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Intermediary obj folder
obj:
	mkdir -p obj

# Cleanup intermediary files
clean:
	rm -R obj

# Board de/allocation and print implementation file
obj/board.o: board/board.cpp $(BOARD_DEP)
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Board placement functions implementation file
obj/board_place.o: board/board_place.cpp $(BOARD_DEP)
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Board piece movement functions implementation file
obj/board_move.o: board/board_move.cpp $(BOARD_DEP)
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Board state retrieveal implementation file
obj/board_state.o: board/board_state.cpp $(BOARD_DEP)
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# Human player implementation file
obj/human.o: utils/human.cpp utils/human.hpp obj
	$(CXX) $(CXXFLAGS) -o $@ -c $<